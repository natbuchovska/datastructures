package com.company;

import java.util.Arrays;

public class Main {


    public static Person[] initArray()
    {
        Person[] list = {
                new Person("Mark", "Brooker", 5000, true),
                new Person("Mary", "Jones", 2000, false),
                new Person("Andy", "Parker", 2500, false),
                new Person("Tyler", "Stevens", 1500, false),
                new Person("Cathy", "Cook", 5050, true),
                new Person("Stephanie", "Gagnon", 2000, true),
                new Person("Alex", "Mudd", 4500, false),
                new Person("Gabriel", "Lee", 6000, true),
        };
        return list;
    }

    public static void sortPeople(Person[] list)
    {
        Arrays.sort(list);
    }

    public static void printFiveTop(Person[] list)
    {
        for (int i = 0; i < 5; i++)
        {
            if (list[i].getSalary() >= 3000 && list[i].isMarried())
            {
                System.out.println(list[i]);
            }
        }
    }

    public static void main(String[] args) {

    Person[] people = initArray();
	sortPeople(people);
	printFiveTop(people);
    }
}

/*Create an array of 8 Employee with name, family, salary and a boolean isMarried.
Employee("name","family",11000,true);

Show list of 5 married persons with salary more than 3000 in descending order.*/
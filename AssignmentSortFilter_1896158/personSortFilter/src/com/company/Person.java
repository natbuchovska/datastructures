package com.company;

public class Person implements Comparable<Person>{


    public Person(String name, String family, double salary, boolean isMarried) {
        this.name = name;
        this.family = family;
        this.salary = salary;
        this.isMarried = isMarried;
    }
    private String name;
    private String family;
    private double salary;
    private boolean isMarried;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public boolean isMarried() {
        return isMarried;
    }

    public void setMarried(boolean married) {
        isMarried = married;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", family='" + family + '\'' +
                ", salary=" + salary +
                ", isMarried=" + isMarried +
                '}';
    }

    @Override
    public int compareTo(Person o) {
        return (int)(o.salary - this.salary); //descending order
    }
}

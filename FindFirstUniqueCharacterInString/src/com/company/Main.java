package com.company;

public class Main {

    public static int findFirstUniqueCharacter(String s)
    {
        boolean isUnique = false;
        int index = 0;
        for (int i = 0; i< s.length(); i++)
        {

            for (int j = i+1; j< s.length(); j++ )
            {
                if (s.charAt(i) == s.charAt(j))
                {
                    isUnique = false;
                    break;
                }
                else
                {
                    isUnique = true;
                }

            }
            if (!isUnique)
                continue;
            else
            index = i;
            break;
        }

        return index;
    }


    public static void main(String[] args) {
	int uniCharacterIndex = findFirstUniqueCharacter("ttthyyy");
	System.out.println(uniCharacterIndex);
    }
}

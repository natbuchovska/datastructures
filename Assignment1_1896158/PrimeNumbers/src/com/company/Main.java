package com.company;

public class Main {

    public static boolean checkIfPrimeNumber(int num)
    {
        for (int i = 2; i <= num/2; i++)
        {
            if (num%i == 0)
                return false;
        }
        return true;
    }

    public static void main(String[] args) {
        if (checkIfPrimeNumber(17))
        {
            System.out.println("Prime number.");
        }
        else
        {
            System.out.println("Not a prime number.");
        }
    }
}

package com.company;

public class Main {

    public static String convertToHex(int decNum)
    {
        String result = "";

        while (decNum > 0)
        {
            int remainder = decNum%16;
            if (remainder < 10)
            {
                result = (char)(remainder+48)+result;
            }
            else {

                result = (char)(remainder+65-10)+result;
            }
            decNum = decNum/16;
        }
        return result;
    }

    public static void main(String[] args) {
        String hex = convertToHex(1728);
        System.out.println("Result: "+hex);
    }
}

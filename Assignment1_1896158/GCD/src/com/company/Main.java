package com.company;

public class Main {

    public static int gcdCalculator(int n1, int n2) {
        if (n2 == 0) {
            return n1;
        } else {
            return gcdCalculator(n2, n1 % n2);
        }
    }

    public static void main(String[] args) {
        int gcd = gcdCalculator(55, 121);
        System.out.println("GCD of given numbers is: " + gcd);

    }
}

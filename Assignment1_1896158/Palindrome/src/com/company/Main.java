package com.company;

public class Main {

    public static boolean checkPalindrome(String s)

    {

        int low = 0;
        int high = s.length()-1;
        while (low < high)  //for( int i = 0; i < s.length()/2; i++ )
        {
           if (s.charAt(low) != s.charAt(high)) //if (str.charAt(i) != str.charAt(n-i-1))
           {
               return false;
           }
               low++;
               high--;
        }
        return true;
    }

    public static void main(String[] args) {
        if (checkPalindrome("manam"))
        {
            System.out.println("String is a palindrome.");
        }
        else
        {
            System.out.println("String is not a palindrome.");
        }
    }
}

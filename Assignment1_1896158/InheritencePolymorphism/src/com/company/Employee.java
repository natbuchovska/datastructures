package com.company;
import java.time.YearMonth;

public abstract class Employee extends Person {
    public Employee()
    {
        System.out.println("This is Employee constructor.");
    }

    public String employementStart;

    //superclass static method hidden
    public static void personCount()
    {
        personAmount = 1;
        System.out.println("Person amount in Employee class: "+ personAmount);
    }

    public abstract void completeDuties(); //subclasses have different implementation of their duties

    public void getYearsOfExperience(Employee e)
    {
        int currYear = YearMonth.now().getYear();
        int yearOfHiring = Integer.parseInt(e.employementStart);
        int yoe = currYear - yearOfHiring;
        System.out.printf("This Employee has %d years of experience.", yoe);
        System.out.println();
    }

    //overriden
    public void canTalk()
    {
        System.out.println("Employee can talk.");
    }
}

package com.company;


public class Main {

    public static void main(String[] args) {
	Secretary s1 = new Secretary();
    Manager m1 = new Manager("John", "Smith");
	s1.firstName = "Maria";
	s1.setAge(35);
	s1.employementStart = "2015";
	s1.manager = m1;
	s1.getYearsOfExperience(s1); //polymorphism
    String managerInfo = s1.getManagerInfo().toString();
        System.out.println(managerInfo);
    String managerName = s1.getManagerInfo(true);
        System.out.println(managerName);


    Employee.personCount(); //accessing redefined static method in Employee class, (static variable value changed)
    Person.personCount(); //accessing hidden static method in Person class


    System.out.println("The new manager's name is "
    + m1.firstName+" "+m1.lastName);
    m1.canTalk();



    }
}

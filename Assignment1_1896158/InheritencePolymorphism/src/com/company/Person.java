package com.company;

public abstract class Person { //to prevent from instantiation
    public Person()
    {
        System.out.println("This is Person constructor.");
    }

    public String firstName;
    public String lastName;
    private int age;
    public static int personAmount = 0;

    public static void personCount()
    {
        System.out.println("Person amount in Person class: "+ personAmount);
    }

    public int getAge()
    {
        return this.age;
    }

    public void setAge(int a)
    {
        this.age = a;
    }

    public void canTalk()
    {
        System.out.println("Person can talk.");
    }
}

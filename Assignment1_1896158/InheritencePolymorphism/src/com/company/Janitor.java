package com.company;

public class Janitor extends Employee {


    @Override
    public void completeDuties() {
        System.out.println("Janitor cleans offices.");
    }
}

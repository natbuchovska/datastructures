package com.company;

public class Secretary extends Employee implements OfficeStaff{
    public Secretary()
    {
        System.out.println("This is Secretary constructor.");
    }

    public Manager manager;

    @Override
    public void completeDuties() {
        System.out.println("Secretary prepares reports and documentation.");
    }

    @Override
    public void hasOffice() {
        System.out.println("Secretary has an office.");
    }

    public Manager getManagerInfo()
    {
        return this.manager;
    }
    //overloading
    public String getManagerInfo(boolean i)
    {
        if (i)
        {
            return this.manager.firstName;
        }
        else
        {
            return this.manager.lastName;
        }
    }
}

package com.company;

public class Manager extends Employee implements OfficeStaff {
    //public Manager() {
        //System.out.println("This is Manager constructor.");
    //}

    public Manager(String fname, String lname)
    {
        super(); //calling superclass constructor
        this.firstName = fname;
        this.lastName = lname;
       System.out.println("This is Manager constructor.");
    }

    public String toString()
    {
        return this.firstName+" "+this.lastName;
    }

    @Override
    public void completeDuties() {
        System.out.println("Manager makes important decisions.");
    }

    @Override
    public void hasOffice() {
        System.out.println("Manager has an office.");
    }
}

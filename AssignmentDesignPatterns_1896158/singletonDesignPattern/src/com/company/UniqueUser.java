package com.company;

public class UniqueUser {
    private static UniqueUser userInstance;
    private UniqueUser() {
    }

    public static UniqueUser getInstance(){
        if (userInstance == null)
        {
            userInstance = new UniqueUser();
            System.out.println("Instance created.");
        }
        else {
            System.out.println("!!!Instance already exists.");
        }
        return userInstance;
    }
}

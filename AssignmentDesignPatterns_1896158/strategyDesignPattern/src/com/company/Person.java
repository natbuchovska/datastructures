package com.company;

public class Person {
    public Person() {
        this.speakLanguage = new SpeakEnglish();
    }  //newly created person object speaks English by default

    private CanSpeak speakLanguage;

    public void setSpeakLanguage(CanSpeak speakLanguage) {
        this.speakLanguage = speakLanguage;
    }

    public void performSpeak()
    {
        speakLanguage.speak();
    }


}

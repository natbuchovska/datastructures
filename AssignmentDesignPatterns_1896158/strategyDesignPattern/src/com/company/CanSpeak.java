package com.company;

public interface CanSpeak {
    public abstract void speak();

}

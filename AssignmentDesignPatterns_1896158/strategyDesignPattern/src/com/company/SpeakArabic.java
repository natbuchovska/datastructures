package com.company;

public class SpeakArabic implements CanSpeak{
    @Override
    public void speak() {
        System.out.println("I speak Arabic.");
    }
}

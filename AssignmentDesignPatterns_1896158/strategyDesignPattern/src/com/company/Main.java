package com.company;

public class Main {

    public static void main(String[] args) {
        Person myPerson = new Person();
        myPerson.performSpeak();


        myPerson.setSpeakLanguage(new SpeakArabic());
        myPerson.performSpeak();

        myPerson.setSpeakLanguage(new SpeakChinese());
        myPerson.performSpeak();
    }
}

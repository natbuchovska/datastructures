package com.company;

public class SpeakEnglish implements CanSpeak{
    @Override
    public void speak() {
        System.out.println("I speak English.");
    }
}

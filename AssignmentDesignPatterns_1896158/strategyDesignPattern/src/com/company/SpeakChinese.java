package com.company;

public class SpeakChinese implements CanSpeak {
    @Override
    public void speak() {
        System.out.println("I speak Chinese.");
    }
}

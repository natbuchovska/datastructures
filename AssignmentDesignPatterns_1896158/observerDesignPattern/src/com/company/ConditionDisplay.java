package com.company;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class ConditionDisplay implements Observer {
    private float temperature;
    private float humidity;
    private float pressure;

    Subject subject;

    public ConditionDisplay(Subject subject) {
        this.subject = subject;
        subject.addObserver(this);  //adding observer to observer list when observer is initialized
    }

    @Override
    public void update(float t, float h, float p) {

        this.temperature = t;
        this.humidity = h;
        this.pressure = p;
        display();
    }

    public void display()
    {
        DecimalFormat df = new DecimalFormat("#");
        df.setRoundingMode(RoundingMode.CEILING);
        System.out.println("Temperature: "+ df.format(this.temperature)+"C\nHumidity: "+df.format(this.humidity)+"%\nPressure: "+df.format(this.pressure)+"mbar");

    }
}

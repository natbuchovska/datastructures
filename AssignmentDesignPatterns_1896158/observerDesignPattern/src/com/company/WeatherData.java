package com.company;


import java.util.ArrayList;

public class WeatherData implements Subject {
    private ArrayList<Observer> observers;
    private float temperature;
    private float humidity;
    private float pressure;

    public WeatherData() {
        observers = new ArrayList<Observer>();
    }

    public float getTemperature() {
        return temperature;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getPressure() {
        return pressure;
    }


//    public void setMeasurements(float temperature, float humidity, float pressure)
//    {
//        this.temperature = temperature;
//        this.humidity = humidity;  ;
//        this.pressure = pressure;
//        notifyObservers();
//    }
    public void setMeasurements()
    {
        this.temperature = (float)Math.random()*40;
        this.humidity = (float)Math.random()*100;  ;
        this.pressure = (float)Math.random()*1083;
        notifyObservers();
    }

    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        int i = observers.indexOf(o);
        if (i >= 0)
        observers.remove(i);
    }

    @Override
    public void notifyObservers() {
        for (Observer ob: observers)
        {
            ob.update(this.temperature, this.humidity, this.pressure);
        }
    }
}

package com.company;

public class Main {

    public static void main(String[] args) {
        WeatherData weather = new WeatherData();
        ConditionDisplay disp = new ConditionDisplay(weather);

        weather.setMeasurements();
        System.out.println("\nWeather data changed.\n");
        weather.setMeasurements();
        System.out.println("\nWeather data changed.\n");
        weather.setMeasurements();


    }
}

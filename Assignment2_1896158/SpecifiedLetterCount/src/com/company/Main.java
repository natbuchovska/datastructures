package com.company;

import java.util.Scanner;

public class Main {

    public static int count(String str, char a)
    {
       int startIndex = 0;
       int count = 0;

       return count(str, a, startIndex, count);
    }

    public static int count(String str, char a, int i, int c)
    {
        int number = c;
        if (i < str.length())
        {
            if (str.charAt(i) == a)
                c++;
            i++;
            return count(str, a, i, c);
        }
        else return number;

    }

    public static void main(String[] args) {
        System.out.println("Please enter a string: ");
        Scanner sc = new Scanner(System.in);

        // String input
        String userInput = sc.nextLine();

        System.out.println("Please enter a letter: ");
        char userLetter = sc.nextLine().charAt(0);
        int result = count(userInput, userLetter);
        System.out.printf("There are %d occurrences of letter %c in your string.", result, userLetter);
    }
}

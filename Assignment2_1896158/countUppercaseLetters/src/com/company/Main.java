package com.company;

import java.util.Scanner;

public class Main {


   public static int countUppercase(String s)
    {
        int startIndex = 0;
        int count = 0;
        return countUppercase(s, startIndex, count);

    }


    public static int countUppercase(String s, int index, int c)
    {
        int number = c;
        if (index < s.length())
        {
          if (s.charAt(index) > 64 && s.charAt(index) < 91)
            {
                c++;
            }
            index++;
            return countUppercase(s, index, c);
        }
        else
            return number;
    }


    public static void main(String[] args) {
	System.out.println("Please enter a string: ");
	Scanner sc = new Scanner(System.in);

        // String input
        String userInput = sc.nextLine();
        int result = countUppercase(userInput);
        System.out.printf("There are %d uppercase letters in your string.", result);
    }
}

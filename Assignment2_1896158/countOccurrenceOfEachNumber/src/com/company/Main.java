package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void countOccurrenceOfNumbers(ArrayList<Integer> list) {
        int firstElement = 0;
        countOccurrenceOfNumbers(list, firstElement);
    }

    public static void countOccurrenceOfNumbers(ArrayList<Integer> list, int index)
    {
        if (index < list.size())
        {
            int count = 0;

            for (int i = 0; i < list.size(); i++) {
                boolean isMatch = false;

                if (list.get(index) == list.get(i)) {
                    isMatch = true;

                }
                if (isMatch)
                {
                    count++;
                }

            }
            System.out.printf("Number %d appears %d times\n", list.get(index), count);
            index++;
            countOccurrenceOfNumbers(list, index);

        }
    }

    public static void main(String[] args) {
        ArrayList<Integer> integerList = new ArrayList<>();
	    System.out.println("Please enter numbers between 1 and 100. Enter 0 to finish input.");
        int n = -1;
        Scanner sc = new Scanner(System.in);

        while(true) {
            n = sc.nextInt();
            if (n != 0)
            {
                integerList.add(n);
            }
            else break;
        }

       /* for (int i =0; i < integerList.size(); i++){
            System.out.println(integerList.get(i));
        }*/
        countOccurrenceOfNumbers(integerList);
    }
}

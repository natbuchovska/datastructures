package com.company;

public class Main {

    public static void randomCard(int[] a1, String[] a2, String[] a3)
    {
       int rand = (int)(Math.random()*a1.length);
       int cardNumber = a1[rand];
       String suit = a2[cardNumber/13];
       String rank = a3[cardNumber%13];
       System.out.printf("The picked card is %s of %s\n", rank, suit);
    }

    public static void main(String[] args) {
        int[] deck = new int[52];
        // Initialize cards
        for (int i = 0; i < deck.length; i++)
            deck[i] = i;

        String[] suitArray = {"Spades", "Hearts", "Diamonds", "Clubs"};
        String[] rankArray = {"Ace", "2","3","4","5","6","7","8","9","10","Jack", "Queen", "King"};

        //Randomly shuffle
        for (int j = 0; j < deck.length; j++)
        {
            int k = (int)(Math.random()*deck.length);
            int temp = deck[j];
            deck[j] = deck[k];
            deck[k] = temp;
        }

        //pick and print random cards
        randomCard(deck, suitArray, rankArray);
        randomCard(deck, suitArray, rankArray);
        randomCard(deck, suitArray, rankArray);
        randomCard(deck, suitArray, rankArray);

    }
}

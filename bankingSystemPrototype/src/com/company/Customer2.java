package com.company;

public class Customer2 implements Observer {

    int index;
    Subject subject;
    TypeOfInvestment ti;

    public Customer2(Subject subject) {
        this.subject = subject;
        subject.addObserver(this);
        System.out.println("Customer 2 is created and added to list of bank observers.");
    }

    public void invest()
    {
        ti.useType();
    }

    @Override
    public void update(int i) {
        this.index = i;
        display();
    }
    public void display()
    {
        System.out.println("Customer2 notified. Index dropped to: "+ this.index);
    }
}

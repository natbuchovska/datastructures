package com.company;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Bank implements Subject {
    private ArrayList<Observer> observers;
    private static Lock lock = new ReentrantLock(); // Create a lock
    private int index = 752858;
    private Bank() {
        observers = new ArrayList<Observer>();
    }

    public static Bank getBankInstance() {
        return bankInstance;
    }

    private static Bank bankInstance;

    public int getIndex() {
        return index;
    }




    public static Bank getInstance(){
        if (bankInstance == null)
        {
            bankInstance = new Bank();
            System.out.println("Bank created.");
        }
//        else {
//            System.out.println("!!!Bank already exists.");
//        }
        return bankInstance;
    }

    public  void setIndex(int newIndex)
    {
        lock.lock(); // Acquire the lock

        if (newIndex < this.index) {
            notifyObservers(newIndex);
        }
        try {
        Thread.sleep(5);
        this.index = newIndex;
        }
        catch(InterruptedException ex){}
        finally {
            lock.unlock(); // Release the lock
        }
    }


    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        int i = observers.indexOf(o);
        if (i >= 0)
            observers.remove(i);

    }

    @Override
    public void notifyObservers(int i) {
        for (Observer ob: observers)
        {
            ob.update(i);
        }

    }
}

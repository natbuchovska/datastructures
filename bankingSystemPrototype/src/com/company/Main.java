package com.company;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {
    //Multithreading
    ExecutorService executor = Executors.newFixedThreadPool(2);
    executor.execute(new ChangeIndexTask());
    executor.execute(new ChangeIndexTask());


        //Singleton
	Bank bank = Bank.getInstance();
	Bank bank1 = Bank.getInstance();//will return the value


    //Strategy
    System.out.println();
	Customer1 c1 = new Customer1(bank);
	c1.ti = new GIS();
	c1.invest();
	c1.ti = new MutualFond();
	System.out.println("Customer 1 changed type of investment.");
	c1.invest();
	Customer2 c2 = new Customer2(bank);

    //Observer
    System.out.println();

    executor.shutdown();



    }
}

package com.company;

public class Person {
    public Person()
    {
        Thread thread1 = new Thread(new Task());
        Thread thread2 = new Thread(new Task2());

        thread1.start();
        thread2.start();

        try {
            thread1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i <250; i++) {
            System.out.println("Thread 0: 0 ");
        }
    }
}

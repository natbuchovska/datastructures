package com.company;

import java.util.Comparator;

public class Person implements Comparable<Person>{
    int id;
    String firstName;
    String lastName;

    public Person(int pId, String fn, String ln)
    {
        this.id = pId;
        this.firstName = fn;
        this.lastName = ln;
    }

    @Override
    public int compareTo(Person o) {
        return this.id - o.id;
    }

    public String toString()
    {
       return this.id + " " + this.firstName +" "+ this.lastName;
    }


<<<<<<< HEAD:Assignment_Sort_Person_1896158/sortPerson/src/com/company/Person.java

    static class InnerPersonById implements Comparator<Person>{
=======
>>>>>>> b385a017f63dd8bc2c5b87df1b4200d4faf25f10:sortPerson/src/com/company/Person.java
        @Override
        public int compare(Person o1, Person o2) {
            return o1.id - o2.id;
        }
    }

    static class InnerPersonByFirstName implements Comparator<Person>{
        @Override
        public int compare(Person o1, Person o2) {
            return o1.firstName.compareTo(o2.firstName);
        }
    }

    static class InnerPersonByLastName implements Comparator<Person> {
        @Override
        public int compare(Person o1, Person o2) {
            return o1.lastName.compareTo(o2.lastName);
        }
    }}

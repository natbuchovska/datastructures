package com.company;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class Main {



    public static void sortWithComparable(Person[] list)
    {
        java.util.Arrays.sort(list);
    }

    public static void sortById(Person[] list)
    {
        Arrays.sort(list, new Person.InnerPersonById());
    }

    public static void sortByName(Person[] list)
    {
        Arrays.sort(list, new Person.InnerPersonByFirstName());
    }

    public static void sortByFamily(Person[] list)
    {
        Arrays.sort(list, new Person.InnerPersonByLastName());
    }

    public static void sortByIdAnonymous(Person[] list)
    {
        Arrays.sort(list, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.id - o2.id;
            }
        });
    }

    public static void sortByNameAnonymous(Person[] list)
    {
        Arrays.sort(list, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.firstName.compareTo(o2.firstName);
            }
        });
    }

    public static void sortByFamilyAnonymous(Person[] list)
    {
        Arrays.sort(list, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.lastName.compareTo(o2.lastName);
            }
        });
    }

    public static void sortByIdLambda(Person[] list)
    {
        Arrays.sort(list, (a,b) -> {return a.id - b.id;});
    }

    public static void sortByNameLambda(Person[] list)
    {
        Arrays.sort(list, (a,b) -> {return a.firstName.compareTo(b.firstName);});
    }

    public static void sortByFamilyLambda(Person[] list)
    {
        Arrays.sort(list, (a,b) -> {return a.lastName.compareTo(b.lastName);});

    }

    public static void printArray(Person[] list)
    {
        for(Person p : list)
        {
            System.out.println(p.toString());
        }
    }

    public static void main(String[] args) {
        Person[] people = {
                new Person(2, "Anna", "Buch"),
                new Person(1, "Mary", "Poppins"),
                new Person(3, "Michael", "Kors"),
                new Person(5, "Britney", "Spears"),
                new Person(4, "Justin", "Bieber")
        };
        // 1. sort using Comparable interface
        sortWithComparable(people);

        // 2. sort using inner classes

        //sortById(people);
        //sortByName(people);
        //sortByFamily(people);

        // 3. sort using Anonymous inner class
        //sortByIdAnonymous(people);
        //sortByNameAnonymous(people);
        //sortByFamilyAnonymous(people);

        // 4. sort using Lambda
        //sortByIdLambda(people);
        //sortByNameLambda(people);
        //sortByFamilyLambda(people);

        //print array
        printArray(people);
    }
}
